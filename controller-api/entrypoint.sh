#!/bin/bash

# Wait for bluetooth address to change
BT_MAC=$(hcitool dev | grep -o "[[:xdigit:]:]\{11,17\}")
if [ "$BT_MAC" = "94:58:CB:44:55:66" ]; then
echo "Bluetooth configured already.";
else
echo "Wait a bit...";
sleep 45;
fi

if [ "$SWITCH_MAC" = "" ]; then
tail -F anything;
else
python3 server.py --service_port 5000 -r $SWITCH_MAC;
fi
