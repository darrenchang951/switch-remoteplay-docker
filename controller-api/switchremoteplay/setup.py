from distutils.core import setup

from setuptools import find_packages

install_requires = [
	'eventlet~=0.30.0',
	'joycontrol @ git+https://github.com/Poohl/joycontrol.git@3e80cb315dab8c2e2daedc80d447836b7d4d85f7',
	'flask-socketio~=5.0.0',
]

test_deps = [
	'pytest',
]

setup(
	name='switch-remoteplay-server',
	version='1.0.0',
	packages=find_packages(),
	url='https://github.com/juharris/switch-remoteplay',
	license='MIT',
	author="Justin D. Harris",
	author_email='',
	description="Play your Nintendo Switch remotely. Run this on a device that can connect to a Switch.",
	install_requires=install_requires,
	tests_require=test_deps,
	extras_require=dict(
			test=test_deps,
	),
)
