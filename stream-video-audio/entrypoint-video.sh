#!/bin/bash

# Stream
(ffmpeg \
-f v4l2 -thread_queue_size 1024 -r 60 -video_size 720x480 -i /dev/video0 \
-f mpegts -codec:v mpeg1video -r 60 -bf 0 -s 720x480 -b:v 1.2M \
http://stream-server:8081/supersecret)
