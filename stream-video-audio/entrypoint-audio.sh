#!/bin/bash

# Stream
ffmpeg -fflags nobuffer -flags low_delay \
-f alsa -thread_queue_size 1024 -ar 44100 -i default \
-f mp2 -codec:a libopus -ar 48000 -ac 1 -b:a 48k \
-muxdelay 0.001 \
http://stream-server:8091/supersecret
