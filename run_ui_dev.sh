#!/bin/bash

docker-compose -f docker-compose-ui.yml build;
docker-compose -f docker-compose-ui.yml up;
docker-compose -f docker-compose-ui.yml down -v;
