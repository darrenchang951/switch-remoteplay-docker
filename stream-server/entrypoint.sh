#!/bin/bash

# Start streaming server (video)
(node websocket-relay.js supersecret 8081 8082) &

# Start streaming server (audio)
(node websocket-relay.js supersecret 8091 8092)
