#!/bin/bash

docker build -t switch_remoteplay/controller-webui-new controller-webui-new/.
docker run --rm -ti \
-v $(pwd)/controller-webui-new/webui/:/source/ \
-p 3000:3000 \
switch_remoteplay/controller-webui-new sh;
