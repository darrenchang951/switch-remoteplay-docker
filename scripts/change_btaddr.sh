#!/bin/bash

# This will probably only work for Raspberry Pi 3
# It prevents random disconnects...

sudo hcitool cmd 0x3f 0x001 0x66 0x55 0x44 0xCB 0x58 0x94
sudo hciconfig hci0 class 0x2508
sudo bdaddr -i hci0 "94:58:CB:44:55:66"
sudo bdaddr -i hci0 -r
sudo hciconfig hci0 reset
sudo systemctl daemon-reload
sudo systemctl restart bluetooth.service
