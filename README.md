# My Switch Project
Remotely play on my nintendo switch

# Prerequisite
1. Setup Bluetooth.
    - change the `ExecStart` parameter in `/lib/systemd/system/bluetooth.service` to
      `ExecStart=/usr/lib/bluetooth/bluetoothd -C -P sap,input,avrcp`.
      This is to remove the additional reported features as the switch only looks for a controller.
      This also breaks all other Bluetooth gadgets, as this also disabled the needed drivers.

2. Add `change_btaddr.sh` script to your crontab so your bluetooth bdaddr and Mac is set to the one that
   seems to work better for Nintendo Switch.
   ```bash
   # Note: you must use sudo
   sudo crontab -e
   ```
   
   Add the following line to the end of the crontab file
   ```bash
   # Note: the 30 seconds delay is a HACK to wait for bluetooth device to become configurable
   @reboot sleep 30 && /<absolute_path_to_the_script>/scripts/change_btaddr.sh
   ```

3. A video capture card plugged into your machine. You should have `/dev/video0`, or both `/dev/video0`
   and `/dev/video1`.

4. Pulse audio installed on the host, if you want to also stream the audio.

5. Increase gpu_mem to at least 128 `gpu_mem=128`. Maybe it helps with the video stream, but MPEG1 is
   actually not hardware accelerated.

# How to
1. Pair your Nintendo Switch, and save the Switches Mac address to `.env` file.
    ```bash
    docker-compose build
    dokcer-compose up -d
    docker exec -ti remoteplay-controller-api bash
    # Inside the container, pair your Nintendo Switch. Once paired, just exit out the container.
    python3 server.py
    ```

2. Recreate controller-api container with the paired `SWITCH_MAC`.
    ```bash
    docker-compose up -d
    ```

# Development
- Use `run_ui_dev.sh` to start a UI development environment.

# Issues
- Sometimes the joycontroller will disconnect. You can restart `controller-api` container to fix it. If
  that doesn't work, you will have to `sudo reboot` your machine. I have no idea why the connection drops.
  https://github.com/Poohl/joycontrol/issues/4

# Acknowledgements
Special thanks 
- Thank you for the virtual controller web socket server and the controler website ui.
    - https://github.com/juharris/switch-remoteplay
- Thank you for providing joycontrol
    - https://github.com/mart1nro/joycontrol (original)
    - https://github.com/Poohl/joycontrol (forked)
- Thank you for providing the code for opening a stream server to HTTP
    - https://github.com/phoboslab/jsmpeg
- Than you for providing the tools for playing audio stream
    - https://www.npmjs.com/package/websocket-audio-api
- Thank you for explaining how pulseaudio thing works, so I can stream audio from a docker container.
    - https://comp0016-team-24.github.io/dev/problem-solving/2020/10/30/passing-audio-into-docker.html
